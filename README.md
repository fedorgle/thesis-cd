# CD

Folder `src` contains sources for my Bachelor's thesis. `impl` contains the jupyter notebooks with code used to preprocess data and train the models and the data used to genreate images.

LaTeX sources with all of the figures are inside the `src/thesis` folder.

The final PDF of the thesis is stored inside the `text` folder.
