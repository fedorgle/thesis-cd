# Dependencies

To run the notebooks, you will need to have an anaconda environment with following dependencies:

- `numpy`
- `pandas`
- `matplotlib`
- `TA-Lib`
- `seaborn`
- `pytorch`
- `torchvision`

`TA-Lib` is additionally required to be installed on your computer, because the python package is simply a wrapper. https://mrjbq7.github.io/ta-lib/install.html

Anaconda should include everything except `TA-Lib` and `PyTorch` by default.  

# Warning

At the moment of writing this, if you want to try out `ConvNeXt`, you need to have nightly version of pytorch.

It can be installed with the following command:

```bash
  conda install pytorch torchvision torchaudio cudatoolkit=11.3 -c pytorch-nightly
```
