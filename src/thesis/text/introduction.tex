% Do not forget to include Introduction
%---------------------------------------------------------------
\chapter*{Introduction}\addcontentsline{toc}{chapter}{Introduction}\markboth{Introduction}{Introduction}
%---------------------------------------------------------------
\setcounter{page}{1}

In this section, we will describe our motivation, the problem we are trying to solve, the goals of the thesis, and the rough structure of the work.

\section*{Motivation}

Forex stands for the foreign exchange market. It is a decentralized global market that allows people to buy and sell different currencies. Forex is the world's largest market with a volume of \$6.6 trillion that determines the foreign exchange rate for every currency. It operates 24h every day except for the weekends, unlike most other markets that are open from 9:30 to 16:00. This also makes it more appealing from the perspective of algorithmic trading.


Forex simultaneously promises an enormous profit and an immense level of risk to any trader. The idea behind Forex is deceivingly simple: buy low, sell high (or the opposite of that), so a trader can profit just by predicting a positive or negative change in the upcoming prices of a currency pair. Nevertheless, the actual prediction of the trend is an exceptionally challenging task, and a very select few are able to do it consistently. That is why a tremendous amount of effort has been put into the Forex price analysis and forecasting research since its inception 50 years ago.

The primary source of our inspiration has been the recent successes of deep learning in image recognition.
So, in this thesis, we will attempt to utilize said successes in a way more lucrative area of Forex and time series forecasting. For our experiments, we have chosen the currency pair with, by far, the most significant volume: EUR/USD.

\section*{Problem statement}

Stock price analysis techniques can generally be split into two broad groups: technical and fundamental analysis. The former focuses on prices themselves, believing that sequences of numbers contain enough information to predict future trends. The latter evaluates stock values by analyzing related economic and financial factors like interest rates, earnings, and Elon Musk’s Twitter.

In our thesis, we will attempt Forex trend prediction, focusing exclusively on technical analysis techniques, more specifically machine learning. Furthermore, since image recognition has been the most successful field for ML in the last decade, we will attempt to apply this success to the lucrative field of time series forecasting.

\section*{Goals}

In this thesis, we are going to aim to achieve the following goals:
\begin{itemize}
  \item Prepare a survey of state-of-the-art image recognition techniques.
  \item Collect and preprocess Forex data set into a labeled image data set.
  \item Prepare image recognition models.
  \item Evaluate the models and describe the result.
\end{itemize}


\section*{Previous work}

Attempts to transform time-series data into images to ease the prediction are not new. Wang \parencite{DBLP:journals/corr/WangO15}
achieved competitive performance by encoding time series into images of Gramian Angular Summation/Difference Fields and Markov Transition Fields. 

An approach spiritually similar to ours was taken by Tsai \parencite{DBLP:journals/corr/abs-1801-03018} by plotting multiple moving averages of the prices and using a convolutional neural network to predict future prices.

Cohen achieved some success in the paper ``Visual Forecasting of Time Series with Image-to-Image Regression'' \parencite{DBLP:journals/corr/abs-2011-09052} by plotting time series directly and training an auto-encoder model on the created images. 

The main downside of these approaches is that they do not take advantage of the additional insights gained by using technical indicators widely used for trading by humans.

This thesis is a continuation of the work conducted by Andrey Babushkin \parencite{babushkin}. We expand on all aspects of the work by using different preprocessing and modeling approaches and focusing on the insight that can be gathered from the images themselves.

\section*{Structure}

In this work, we are going to roughly follow the CRISP-DM \parencite{crisp_dm} standard with the exception of the deployment phase.
So, our thesis is going to be structured as follows:

In the first chapter, we will discuss the business domain of Forex itself, describing the quirks of how Forex trading. Chapter two will cover all the theoretical background necessary for implementing the data preprocessing pipeline. Chapter three will describe the preliminaries necessary to discuss modern models covered in chapter four. As previously stated, chapter four will contain a survey of modern image recognition models. Moreover, in the last chapter, we will describe the data preprocessing pipeline, modeling, and performance of the created models and draw some conclusions.
