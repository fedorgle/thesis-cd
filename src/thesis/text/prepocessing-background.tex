\chapter{Preprocessing background}

In this chapter we describe all the necessary theory, used to implement the preprocessing pipeline described in the chapter 5 of the thesis.

\section{Technical indicators}
\label{sec:techincal_indicators}

As has already been stated in the introduction, this thesis is focused solely on technical analysis. Hayes provides 
a great definition: “Technical analysis is a trading discipline employed to evaluate investments and identify trading opportunities by analyzing statistical trends gathered from trading activity, such as price movement and volume. Unlike fundamental analysis, which attempts to evaluate a security’s value based on business results such as sales and earnings, technical analysis focuses on the study of price and volume.” \parencite{investopedia_tech_def}

Specifically, this section covers technical indicators. Chen describes technical indicators rather comprehensively: “Technical indicators are heuristic or pattern-based signals produced by the price, volume, and/or open interest of a security or contract used by traders who follow technical analysis. By analyzing historical data, technical analysts use indicators to predict future price movements.” \parencite{investopedia_indicators_def}

\subsection{Rate of Change}

Technical indicators can be conventionally split into several categories. One of them is momentum indicators. “Momentum is the difference between two prices taken over a fixed interval” \parencite[][p. 370]{kaufman}. Perhaps the simplest one of them is ROC (Rate of Change). ROC is a pure momentum expressed in percentage. The following formula defines it:


\begin{equation}
    \text{ROC}_{t} = \frac{close_t - close_{t-p}}{close_{t-p}} \cdot 100,    
\end{equation}

where $close_t$ is an asset's close price at index $t$ at which ROC is calculated and $p$ is a parameter that represents the period, or the amount of ticks previous and the current price.

As is seen from the formula, ROC measures the percentage of change between two prices at the end of two periods ~$n$ ticks apart. As for its usefulness, Kaufman states: “Momentum can be used as a trend indicator by selling when the momentum value crosses downward through the horizontal line at zero and buying when it crosses above the zero line.” \parencite[][p. 373]{kaufman}


\subsection{Commodity channel index}

CCI (Commodity Channel Index) is one of the investors’ most popular technical indicators. Donald Lambert initially proposed it in 1980. Schlossberg states that “the prime focus of the CCI was to measure the deviation of the price of the tradable from its statistical average.” \parencite[][p. 91]{schlossberg}.

CCI at index $t$ for some period $p$ is given by equation \ref{eq:cci}.

\begin{equation}\label{eq:cci}
     CCI_t = \text{TP}_t - \frac{\text{SMATP}{p,t}}{0.015} \times \text{MD}_{p, t},
\end{equation}
where $\text{TP}_t$ is the typical price, defined by equation \ref{eq:tp}, $\text{SMATP}_{p, t}$ is simple moving average of the typical price given by equation \ref{eq:smtp} and $\text{MD}_{p, t}$ is mean deviation of the typical price \ref{eq:mdtp}. And $t$ is some index from the data set.

\begin{equation}\label{eq:tp}
     \text{TP}_t = \frac{high_t + low_t + close_t}{3}
\end{equation}

\begin{equation}\label{eq:smtp}
     \text{SMATP}_{p, t} = \frac{1}{p} \sum_{i = 0}^{p} \text{TP}_{t - i}
\end{equation}

\begin{equation}\label{eq:mdtp}
     \text{MD}_{p, t} = \frac{1}{p} \sum_{i = t}^{t - p} |\text{SMATP}_{p, t} - \text{TP}_i|
\end{equation}


\subsection{Relative strength index}

RSI (Relative Strength Index) is also a momentum indicator. J. Welles Wilder Jr. first introduced it in his book “New Concepts in Technical Trading Systems”. It is an indicator that oscillates between the values of 0 and 100. High and low values of RSI are supposed to signify overbought or oversold states of the asset, respectively. What to consider “high” and “low” is arbitrary, but conventionally, thresholds of 70 and 30 are used. Calculation of RSI is a multi-step process described in the section 6 of the book \parencite[][pp. 63-71]{wilder1978}. Computation of RSI with period $p$ at some index $t$, greater than $p$, goes as follows.

\begin{enumerate}
    \item Step is to calculate up and down closes $U_t$ and $D_t$ via equations \ref{eq:rsi_up} and \ref{eq:rsi_down}
\end{enumerate}
\begin{equation}\label{eq:rsi_up}
    U_t = max(0, close_t - close_{t-1})
\end{equation}
\begin{equation}\label{eq:rsi_down}
    D_t = -min(0, close_t - close_{t-1})
\end{equation}

\begin{enumerate}[resume]
    \item Calculate the RS (Relative Strength factor) according to the equation \ref{eq:rsi_rs}
\end{enumerate}

\begin{equation}\label{eq:rsi_rs}
    \text{RS}_{p, t} = \frac{\bar{U}_{M, t}^{p}}{\bar{D}_{M, t}^{p}},
\end{equation}
where $\bar{U}_{M, t}$ and $\bar{U}_{M, t}$ are smoothed moving averages of the up and down closes defined recursively as
\begin{equation}
    \begin{split}
    \bar{U}_{M, t}^{p} = \frac{(p - 1)\bar{U}_{M, t-1}^{p} + y_t}{p}
    \end{split}
\end{equation}
\begin{equation}
    \begin{split}
    \bar{D}_{M, t}^{p} = \frac{(p - 1)\bar{D}_{M, t-1}^{p} + y_t}{p}
    \end{split}
\end{equation}
Initial values of smoothed moving averages $\bar{D}_{M, p+1}^{p}$ and $\bar{D}_{M, p+1}^{p}$ are given by
\begin{equation}
     \bar{U}_{M, p+1}^{p} = \frac{1}{p} \sum_{i = 0}^{p} \text{U}_{t - i}
\end{equation}
\begin{equation}
     \bar{D}_{M, p+1}^{p} = \frac{1}{p} \sum_{i = 0}^{p} \text{D}_{t - i}
\end{equation}

\begin{enumerate}[resume]
    \item Convert the $\text{RS}_t$ into $\text{RSI}_t$ via equation \ref{eq:rsi_rsi}
\end{enumerate}

\begin{equation}\label{eq:rsi_rsi}
    \text{RSI}_{p, t} = 100 - \frac{100}{1 + \text{RS}_{p, t}}
\end{equation}

\subsection{Ultimate Oscillator}

The Ultimate Oscillator is a technical indicator that is quite similar to RSI. It also signifies that an asset is overbought or oversold when it crosses an upper or lower threshold, respectively. It was first created by Larry Williams in 1976 and published in 1985. The core advantage of Ultimate Oscillator over the other indicators is that it considers momentum over three different time frames, typically 7, 14, and 28 ticks. Computation of the Ultimate Oscillator described in the original article \parencite{williams1985ultimate}, for time frame parameters $p_1$, $p_2$, $p_3$ and some index $t$, greater than the largest of parameters, is defined as
\begin{equation}
    \text{UltOsc}_t = 100 \cdot \frac{4 \cdot avg_{p_1, t} + 2 \cdot avg_{p_2, t} + avg_{p_3, t}}{4 + 2 + 1},
\end{equation}
where 
\begin{equation}
    bp_t = close_t - min(low_t, close_{t - 1})
\end{equation}

\begin{equation}
   tr_t = max(high_t, close_{t - 1}) - min(low_t, close_{t - 1})
\end{equation}

\begin{equation}
     avg_{p, t} = \sum_{i=0}^{p} \frac{bp_{t-i}}{tr_{t-i}}  
\end{equation}


\subsection{Stochastic oscillator}

Stochastic oscillator is also a momentum indicator developed by George Lane in the late 1950s. In its principle, it compares the prices of an asset to the range of its prices over some time \parencite{investopedia_stoch}. Value of Stochastic oscillator at tick $t$ with fast period $p$ and slow period $n$ is computed the following way:
\begin{equation}
    L_{p, t} = min(close_{t - 1}, close_{t - 2}, ..., close_{t - p})
\end{equation}
\begin{equation}
    H_{p, t} = max(close_{t - 1}, close_{t - 2}, ..., close_{t - p})
\end{equation}
\begin{equation}
    \text{\%}K_{p, t} = \frac{close_t - L_{p, t}}{H_{p, t} - L_{p, t}} \times 100
\end{equation}
\begin{equation}
    \text{\%}D_{n} = \frac{1}{n}\sum_{i = 0}^{n} \text{\%}K_{p, i},
\end{equation}
where \%$K$ is the value of the fast stochastic oscillator, and \%$D$ is the value of the slow stochastic oscillator. In the provided formula, a slow stochastic oscillator is a simple average of $n$ last \%$K$s, but an exponential average is sometimes used instead.

\subsection{Aroon oscillator}

Aroon indicator was proposed by Tushar Chande. It is a technical indicator that signifies the strength of a trend. There are two kinds of Aroon indicators: high and low. The Aroon-Up indicator measures the number of ticks since the last high for some period of time. Aroon-Down works analogously with the lows.

The Aroon oscillator is simply the difference between the values of Aroon-Up and Aroon-Down indicators. Therefore, it can be used to gauge the strength of the current trend and the likelihood of its change \parencite{investopedia_aroon}. Here is how it is calculated for some period $p$ at index $t$ greater than $p$:

\begin{equation}
    AroonUp_{p, t} = \frac{p - \text{Ticks since p-tick high}}{ p } \times 100
\end{equation}
\begin{equation}
    AroonDown_{p, t} = \frac{p - \text{Ticks since p-tick low}}{ p } \times 100
\end{equation}
\begin{equation}
    AroonOsc_{p, t} = AroonUp_{p, t} - AroonDown_{p, t}
\end{equation}


\subsection{Chande Momentum Oscillator}

Change momentum oscillator (CMO) is another oscillator proposed by Tushar Chande in his book “The New Technical Trader” \parencite[][pp. 93-94]{chande1994}. It is a momentum oscillator that is supposed to mitigate the weaknesses present in RSI and other momentum indicators. Calculation of CMO for two period $p_1$ and $p2$ at some index $t$, greater than $p_1$ and $p_2$, is given

\begin{equation}
    \text{CMO}_{p_1, p_2, t} = 100 \times \frac{\sum_{i=0}^{p_1} U_{t - i} - \sum_{i=0}^{p_2} D_{t - i}}{\sum_{i=0}^{p_1} U_{t - i} + \sum_{i=0}^{p_2} D_{t - i}},
\end{equation}
where $U_t$ and $D_t$ are up and down closes defined in equations \ref{eq:rsi_up} and \ref{eq:rsi_down}.

This indicator is very similar to RSI, but its values are not smoothed, and CMO oscillates between +100 and -100.


\subsection{Absolute price oscillator}
Absolute price oscillator is a relatively simple technical indicator. It is just a difference between an asset’s two close EMAs (Exponential Moving Average). APO crossing above zero is supposed to signify an upward trend, and analogously, crossing below zero means a downward trend. It can be computed trivially for some two periods $P$ and $T$ as

\begin{equation}
    \text{APO}_{p_1, p_2, t} =  S_{p_1, t} - S_{p_2, t}    
\end{equation}
where $S_{p, t}$ is am EMA of the close prices with some period $p$ at index $x$ and a smoothing factor $\alpha$ between 1 and 0, is given by
\begin{equation}
    S_{p, x} =
    \begin{cases}
        close_1 & \text{if $x = 1$}, \\
        \alpha \times close_x + (1 - \alpha)S_{p, x} & \text{if $x > 1$}. \\
    \end{cases}
    \label{eq:step_func}
\end{equation}


\section{Discrete Fourier transform}
\label{sec:ft}
Fourier analysis is a significant scientific achievement widely used in engineering, mathematics, physics, and finance. In addition, it is essential to modern technology in fields like signal processing. Fourier analysis is based on the research of famous french mathematician Jean Baptiste Fourier, whose name it inherited.

This short introduction to DFT is condensed from the chapter 10 of the book ``Introduction to biomedical engineering'' \parencite[][pp. 576-579]{enderle2012introduction} by John Enderle.

Any finite discrete signal can be decomposed into a sum of sine or cosine waves. Discrete Fourier Transform is a linear transformation, that computes a set of coefficients necessary for all the simple waves to add up to the original signal.
It essentially converts a series of equidistant signal samples from the time domain to the frequency domain. Given an input sequence $x(n)$ DFT is defined as

\begin{equation}
        X(m) = \sum_{k=0}^{N-1} x(k)e^{-j\frac{2\pi mk}{N}}; m = 0, 1,...,\frac{N}{2},
\end{equation}
where $x(k)$ represents signal samples, $k$ is the discrete time variable and $N$ is the number of samples of $x(k)$ and $m$ is an index.

And the inverse of the DFT that converts a signal from the frequency domain back to the time domain is defined as

\begin{equation}
    x(k) = \frac{1}{N} \sum_{m=0}^{N-1} X(m) e^{j \frac{2\pi mk}{N}}; k = 0, 1,...,N-1
\end{equation}

In practice, instead of using DFT, a more efficient implementation called FFT (Fast Fourier Transform) is used instead. The outputs of FFT and DFT are identical, but FFT possesses a much faster execution speed.

\section{Filtering}

Filter is a concept borrowed from the field of signal processing. The term itself signifies a process that removes unwanted details or noise from a signal. 

\subsection{Butterworth filter}
\label{sec:butter}

Butterworth filter is a family of filters that belongs to a broader group of linear continuous-time filters. These filters’ primary purpose is to remove some frequencies from the input signal. Butterworth filter was proposed by British physicist Stephen Butterworth in his paper “On the Theory of Filter Amplifiers “\parencite{butterworth1930theory}.

The amplitude response of low-pass Butterworth is given by

\begin{equation}
   |H(j\omega)| = \frac{1}{1 + (\frac{\omega}{\omega_{C}})^{2n}},
\end{equation}
where $\omega_{C}$ is the cut-off frequency, and $n$ is the order of the filter.

Butterworth filter was designed for maximal flatness of frequency response. An in-depth explanation of signal analysis is way outside of the scope of this thesis. The flatness of frequency response can be thought of as the purity of the signal. A more detailed and formal explanation is provided by Mathworks \parencite{matlab_butter}.
