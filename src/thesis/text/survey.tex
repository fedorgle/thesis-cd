\chapter{Survey}
\label{ch:survey}
In this chapter, we will describe modern methods employed in image recognition. We will describe the specific influential models in chronological order, focusing on more general innovations and improvements proposed with these models.

\section{ImageNet}

The whole field owes a great deal of its success to the famous ILSVRC \parencite{ILSVRC15} competition. ILSRVC stands for ImageNet Large Scale Visual Recognition Challenge. The competition challenges machine learning engineers to train the best model using the ImageNet data set. 
ImageNet is a gigantic database of images, containing hundreds of thousands of images for every 22,000 categories (15 million images in total). ImageNet has been for a decade and still is the de facto benchmark used to generally evaluate the performance of any image recognition approach and assign it the status of state-of-the-art.


\section{AlexNet}

AlexNet \parencite{krizhevsky2012imagenet}, created by Alex Krizhevsky, was the winning model of ILSVRC-2012 competition. It was the first deep learning model to win the competition and attracted much attention to deep learning by greatly outperforming all traditional approaches.

AlexNet is a convolutional neural network with eight layers in total first 5 of which are convolutional and pooling, and the remaining three are fully connected. The architecture is better described by the figure from the original paper \ref{fig:alex_arch}.

While AlexNet is not as impressive now as it was ten years ago, it set the framework, of stacking 2 to 3 convolutional layers followed by a pooling layer for most CNN architectures onward. It still can show great results on simpler kinds of images.

\begin{figure}
    \centering
    \includegraphics[width=12cm]{figures/alexnet_arch.png}
    \caption{AlexNet architecture}
    \label{fig:alex_arch}
\end{figure}

\subsection{Going deeper}

ILSVRC-2014 was dominated by two models: VGG \parencite{vgg} and GoogLeNet \parencite{szegedy2015going}. Both of these models consolidated the success of AlexNet by becoming significantly deeper and wider. Both models are large: VGG consists of 16 layers, and GoogLeNet has 22, but these models still follow the general framework set by AlexNet.

\begin{figure}[h]
    \centering
    \includegraphics[width=12cm]{figures/inception_mod.png}
    \caption{Inception module from GoogLeNet}
    \label{fig:inception}
\end{figure}


Apart from being significantly larger, GoogLeNet introduced a novelty in the form of the Inception module that allowed running multiple convolution and pooling operations with different filters in parallel, therefore avoiding common trade-offs faced by the prior architectures \parencite{alto_googlenet}. An Inception module is illustrated in figure \ref{fig:inception} by Alto \parencite{alto_googlenet}.


\section{ResNet}

ResNet \parencite{resnet} is a CNN architecture that won 2015 ILSVRC competition. ResNet introduced the concept of \textit{residual learning} that significantly mitigates the vanishing gradient problem that plagued networks with a high number of layers.

\begin{figure}[h]
    \centering
    \includegraphics[width=6cm]{figures/residual_block.png}
    \caption{A single residual block}
    \label{fig:res_block}
\end{figure}

\textit{Residual learning} is implemented by the addition of shortcut connections between the layers of the network. These connections simply add inputs of a layer to the inputs of another layer deeper in the model \parencite{resnet}. A single such connection can be seen in figure \ref{fig:res_block} from the original paper. This novelty made it possible to train models with hundreds of layers successfully.

While ResNet is outperformed by models discussed further in the thesis on the ImageNet benchmark, it stays the industry standard. As a result, it is often selected as a first model in the experiments or as a baseline for a newly proposed architecture.

\section{DenseNet}

DenseNet \parencite{densenet} is a successful architecture inspired by ResNet. It takes \textit{residual learning} to the extreme by densely connecting all layers in the neural network, hence DenseNet. In other words, in DenseNet, the input of every hidden layer is formed by concatenating the output of the previous layer with the inputs of all of the layers before it, as seen in figure \ref{fig:dense_arch} by Huang \parencite{densenet}.

\begin{figure}[h]
    \centering
    \includegraphics[width=7cm]{figures/densenet_arch.png}
    \caption{DenseNet architecture}
    \label{fig:dense_arch}
\end{figure}

DenseNet achieves competitive performance with the drawback of requiring significantly more GPU memory to train.

\section{Vision Transformer}

Vision Transfomer or ViT is the only transformer model on this list. It was first introduced in the paper ``An Image is Worth 16x16 Words: Transformers for Image Recognition at Scale'' by Alexey Dosovitsky \parencite{vit}. While it is not the first attempt at applying immensely popular transformers to image recognition, it is undoubtedly the first successful one, beating ImageNet's state-of-the-art performance at the time.

\begin{figure}[h]
    \centering
    \includegraphics[width=14cm]{figures/arch_vit.png}
    \caption{ViT architecture}
    \label{fig:arch_vit}
\end{figure}

The name of the paper is a reference to the model's architecture, displayed in the figure \ref{fig:arch_vit}, taken from the paper. The model splits input images into fixed-size patches (e.g., 16$\times$16). These patches are then linearly embedded, and a position embedding is added to them so that the models have an understanding of where these patches are in the image.
With an additional learnable embedding prepended to them, the resulting vectors are then fed into the transformer encoder.

The encoder consists of alternating layers of multiheaded self-attention and multilayer perceptron blocks, both of which we have discussed in the previous chapter, with Layernorm \parencite{ln} normalization applied before every block. Finally, the output of the encoder is passed into a multilayer perceptron that does the classification.

\section{ConvNeXt}

ConvNeXt \parencite{convnext} is a CNN that Takes ResNet as a base and heavily modifies it by applying some modern techniques inspired by transformers. ConvNeXt is partially inspired by the paper ``ResNet strikes back: An improved training procedure in timm'' \parencite{resnet_strikes_back}, which demonstrates that ResNet can achieve much greater performance when trained with the use of more modern techniques.

ConvNext is trained with AdamW \parencite{adamw} optimizer on data augmented with the newer techniques. They also swapped out heavily overlapping 7$\times7$ convolutions with a stride of 2 at the beginning of the network for non-overlapping 4$\times$4 layers with a stride of 4, mimicking the split into patches done by transformers like ViT.

\begin{figure}[h]
    \centering
    \includegraphics[width=7cm]{figures/convnext_block.png}
    \caption{ResNet and ConvNext blocks comparison}
    \label{fig:convnext_block}
\end{figure}

Figure \ref{fig:convnext_block} is part of the diagram from the original paper that compares blocks of ResNet and ConvNeXt.
As seen in the figure, blocks use larger 7$\times$7 kernels in the convolutional layers. Inverted bottlenecks, also typical in transformers, were introduced to the blocks of the ConvNext. An inverted bottleneck is a sequential pair of layers where the dimension of the first layer is significantly smaller than the dimension of the second one. In the case of ConvNext, it is four times. 

Just like transformers, ConvNeXt has fewer activation functions and fewer normalization layers. Also traditionally used in CNNs ReLU activation function and BatchNorm \parencite{batchnorm} normalization are replaced with GELU \parencite{gelu} activation and simpler Layer Normalization \parencite{ln}.
ConvNeXT also has separate downsampling layers.
