\chapter{Deep learning background}

This chapter will discuss the theoretical background on which models from the following chapter are based. 

\section{Basics}

Before diving deeper into the more advanced architectures like convolutional neural networks and transformers, we will address the basics to lay the necessary foundation to discuss modern neural network architectures.

\subsection{Single-layer perceptron}
\label{sec:neuron}

Single-layer perceptron is the simplest kind of artificial neural network (ANN), that was developed by Frank Rosenblatt in the 1950s. It takes $n$ inputs $\boldsymbol{x} = (x_1, ..., x_n)^T$ and produces a single output $\hat{Y}$. Single-layer perceptron has $n$ real parameters called weights, denoted $\boldsymbol{w} = (w_1, ..., w_n)^T$, one for each input, and an additional real parameter $b$ called bias. Inner potential $\xi$ of a perceptron is given by equation \ref{eq:neuron}.

\begin{equation}
    \xi = \sum_{i=1}^{n} w_i x_i + b = \boldsymbol{w}^T\boldsymbol{x} + b,
    \label{eq:neuron}
\end{equation}
and the output of the perceptron is defined by equation \ref{eq:neuron_output}.

\begin{equation}
    \hat{Y} = f(\xi),
    \label{eq:neuron_output}
\end{equation}
where $f$ is a activation function, in the simplest case, a step function, defined in equation \ref{eq:step_func}, but it can be replaced with any non-linear function  \parencite{vzd_nn}.

\begin{equation}
    f(\xi) =
    \begin{cases}
        1 & \text{if $\xi \geq 0$}, \\
        0 & \text{if $\xi < 0$}. \\
    \end{cases}
    \label{eq:step_func}
\end{equation}

Single-layer perceptron is not particularly interesting or powerful on its own, as it is famously incapable of implementing an XOR function. However, it is a building block that makes more capable models possible.

\subsection{Multilayer perceptron}

Multilayer perceptron (MLP) is a quantitative extension of the single-layer one. MLP is a model that consists of $l$ layers of neurons, as seen in figure \ref{fig:mlp}. Each layer can contain an arbitrary number of neurons. We will denote the size of each layer as $n_1, ... n_l$ and the input size of the whole network as $n_0$.

An output of $j$th neuron in $i$th layer can be represented as a function \[g_j^{(i)}: \mathbb{R}^{n_i - 1} \rightarrow \mathbb{R}, \] that receives $n_{i-1}$ inputs from the previous layer. The output of $g_j^{(i)}$ is computed identically to equations \ref{eq:neuron} and \ref{eq:neuron_output}.

A whole $i$th layer of the network can be denoted as \[ g^{(i)}: \mathbb{R}^{n_i - 1} \rightarrow \mathbb{R}^{n_i}, \], where $g^{(i)} = (g_1^{(i)}, ..., g_{n_i}^{(i)})^T$. And, of course, the whole neural network can be represented as a function \[g: \mathbb{R}^{n_0} \rightarrow \mathbb{R}^{n_l} \], that is given by composition of all the layers in the network: $g = f^{(l)} \circ f^{(l - 1)} \circ ... \circ f^{(2)} \circ f^{(1)} $ \parencite{vzd_nn}.

\begin{figure}[h]
    \centering
    \includegraphics[width=7cm]{figures/nn.png}
    \caption{A diagram of a basic MLP}
    \label{fig:mlp}
\end{figure}


\subsection{Activation functions}

We have already glanced upon activation functions but have not yet given them the attention they deserve. As stated in  section \ref{sec:neuron} any non-linear function can serve as activation function. They serve a crucial role in neural networks since they introduce non-linearity. A whole MLP without activation functions could be replaced by single matrix multiplication.

Different layers may use different activation functions, but, in practice, the most common activation function for hidden layers (the ones that are neither input nor output) is ReLU \ref{eq:relu}.

\begin{equation}
    f(x) = max(0, x)
    \label{eq:relu}
\end{equation}

The choice of activation function for the output layer largely depends on the task. For $c$ class classification \textit{softmax} is the most popular option. It is described in the equation \ref{eq:softmax}. Moreover, no activation function is used in the last layer for regression tasks.

\begin{equation}
    f_i(\boldsymbol{\xi}) = \frac{e^{\xi_i}}{e^{\xi_1} + ... + e^{\xi_c}},
    \label{eq:softmax}
\end{equation}
where $\boldsymbol{\xi} = (\xi_1, ... , \xi_c)^T$ is a vector of inner potentials of $c$ neurons and $f_i(\boldsymbol{\xi})$ is the activation function $i$th neuron \parencite{vzd_nn}.

\subsection{Cost function}

To train a neural network, we need to have some notion of how well the model performs. This is achieved by introducing a cost (often called loss) function.

There are multiple cost functions, each with applications, drawbacks, and advantages. For classification tasks the standard one is \textit{cross-entropy}. For classification of $c$ classes, \textit{cross-entropy} is given by

\begin{equation}
    L(Y, \boldsymbol{\hat{p}}) = - \sum_{j = 1}^{c} \mathds{1}_{Y = j} \log \hat{p}_j = - \log \hat{p}_Y,
\end{equation}
where $\hat{p}_{i} = \hat{P}(Y = i| X = x)$ and $\boldsymbol{\hat{p}} = (\hat{p}_1, ..., \hat{p}_c)^T$ \parencite{vzd_nn}.

\section{Optimizers}

There are several different algorithms for training neural networks called optimizers. An optimizer is an algorithm that updates model parameters during training. While all optimizers diverge in details and often address different nuances of the training process, the core idea is the same. Every optimizer is a modification of the \textit{gradient descent} algorithm. 

\subsection{Gradient descent}

Gradient descent is the algorithm that allows neural networks to learn. The goal of gradient descent is to iteratively minimize the average output of the cost function for every item in the training set. To achieve that, we need to compute the vector of partial derivatives of the cost function $C$ with respect to each weight and bias in the network called \textit{gradient}. That is where the algorithm gets its name. \textit{Gradient} is denoted as $\nabla C$ as seen in equation \ref{eq:gradient}.

\begin{equation}
    \nabla C = \left( \frac{\partial C}{\partial w^{(1)}}, \frac{\partial C}{\partial b^{(1)}}, ... , \frac{\partial C}{\partial w^{(l)}}, \frac{\partial C}{\partial b^{(l)}} \right),
    \label{eq:gradient}
\end{equation}
where $w^{(1)}, ... , w^{(l)}$ are all the weight vectors and $b^{(1)}, ... , b^{(l)}$ are all the bias vectors for each layer in the network.

After computing the \textit{gradient}, it is multiplied by \textit{learning rate} and subtracted from the weights and biases in the network, thus slightly nudging the network towards a better solution. \textit{Learning rate}, often denoted as $\eta$, is one of the essential hyperparameters of a network. It decides how much the network's weights and biases get updated in each iteration. A network with a too high value of $\eta$ can skip optimal weight values, and a network with a \textit{learning rate} set too low can train forever. Therefore $\eta$ must be selected carefully when training a neural network.

\subsection{Stochastic gradient descent}

Neural networks nowadays are trained on big data sets. Therefore precise calculation of the \textit{gradient} can become too computationally intensive even for modern hardware. Stochastic gradient descent (SGD) is a modified gradient descent algorithm designed to mitigate that. 

SGD simplifies the task of calculating \textit{gradient} by approximating it. Instead of computing the cost for all entries in the training set, SGD computes \textit{gradient} and updates the model's parameters for entry in the training set separately. The resulting gradients and, thus, parameter updates are less representative of the actual data. However, the error is considered negligible in light of the speed-up achieved by this approach. It was shown that, when we slowly decrease \textit{leaning rate}, SGD demonstrates same convergence behavior as the classic \textit{gradient descent} \parencite{optimzer_overview}.

The mini-batch gradient descent is a middle ground between SGD and the original algorithm. Instead of computing gradient for every entry in the training set, it randomly splits the training data into fixed-size batches and performs parameter updates for each batch. As a result, this approach achieves a more precise gradient approximation than the SGD and is less computationally intensive than the gradient descent algorithm. Conventionally, mini-batch gradient descent is also referred to as SGD.

\subsection{Adam}

Adam is considered the sane default optimizer for most neural networks and types of data sets. Adam was proposed by Diederik in the paper ``Adam: A Method for Stochastic Optimization'' \parencite{adam}. Adam's approach is inspired by two other optimizers, Adagrad \parencite{adagrad} and RMSprop (RMSprop is an unpublished algorithm, but it is covered by Ruder \parencite{optimzer_overview}), and it combines ideas from both of them.

Adam stores an exponentially decaying average of past squared gradients $v_t$ and an exponentially decaying average of past gradients $m_t$ u, both of which are initialized with vectors of zeroes and are updated at each iteration $t$ as defined in equation \ref{eq:adam_update}.

\begin{equation}
    \begin{aligned}
    m_t = \beta_1 m_{t - 1} + (1 - \beta+1)\nabla C_{t - 1}  \\
    v_t = \beta_2 m_{t - 1} + (1 - \beta+1)(\nabla C_{t - 1})^2,
    \end{aligned}
    \label{eq:adam_update}
\end{equation}
where $\beta_1$ and $\beta_2$ are optimizer hyper parameters.

To combat the bias, introduced by parameters' initialization with zeroes, the $v_t$ and $m_t$ are corrected as described in equation \ref{eq:adam_correct}.

\begin{equation}
    \begin{aligned}
    \hat{m_t} = \frac{m_t}{1 - \beta_1^t}  \\
    \hat{v_t} = \frac{v_t}{1 - \beta_2^t},
    \end{aligned}
    \label{eq:adam_correct}
\end{equation}

Lastly, both parameters are used to update the network's parameters according to Adam's update rule from equation \ref{eq:adam_update_rule}.


\begin{equation}
    \theta_{t + 1} = \theta_t - \frac{\eta}{\sqrt{\hat{v_t}} + \epsilon} \hat{m}_t,
    \label{eq:adam_update_rule}
\end{equation}
where $\theta_t$ denotes all parameters of the network at iteration $t$, $\eta$ is the learning rate and $\epsilon$ is simply a very small number \parencite{optimzer_overview}.

\section{Convolutional neural network}

Convolutional neural networks (CNN) are a class of artificial neural networks that can be described as a regularized version of multilayer perceptron. CNN's are comprised of convolutional layers and pooling layers, which we will discuss further in this section, and, typically, some fully connected layers at the end of the network.

CNN's are most commonly applied to visual data. Unlike MLPs, CNNs are able to preserve some spatial structure from the input. Therefore they tend to perform significantly better in most image recognition tasks, where this kind of information is crucial.

For example, if we wanted to solve an image classification task with the help of an MLP, we flatten the image to pass it as input to the first layer. As a result, the network receives a one-dimensional vector of pixel values, thus losing access to all of the spatial structure that was present in the original image.

\subsection{Convolution operation}
\label{sec:conv}

Convolution is an operation performed on images with the use of a smaller weights matrix called kernel or \textit{filter}. The kernel is slid systematically over all overlapping kernel-sized patches of the image to produce the output. The element-wise product between each patch and kernel is computed and summed. Values of these sums are the output of the convolution operation. The whole process is better illustrated by figure \ref{fig:convolution_diagram}, taken from RiverTail \parencite{rivertail_docs} documentation. Convolution operation has an important quirk: its output is slightly smaller than the input image.

\begin{figure}[h]
    \centering
    \includegraphics[width=8cm]{figures/convolution_diagram.png}
    \caption{Visualization of a convolution operation.}
    \label{fig:convolution_diagram}
\end{figure}

\subsection{Convolutional layer}

As is evident from the name, the convolutional layer employs a convolution operation. Almost always, it employs several of them. Each layer can have multiple \textit{filters}. Each \textit{filter} is a matrix of weights that are optimized during the training process of the network. Every \textit{filter} is used to convolve the input of the layer separately. Results of these convolutions are called \textit{feature maps}. So, a single convolutional layer outputs a volume of \textit{feature maps}: one for each filter it contains, as seen in figure \ref{fig:conv_volume}.

There is a single difference between the convolution operation that we described in section \ref{sec:conv} and the one used inside CNNs: the latter also adds a bias to the weighted sum. After computing the linear transformation that is the convolution, the activation function is applied to each \textit{feature map} analogously to MLPs.

The convolutional layer has three hyper-parameters: a number of filters, filter size, and stride. The number of filters is arbitrary as it defines the depth of the output. Filter size is typically kept between $3 \times 3$ and $7 \times 7$. Stride is the step with which filters are moved across the input. It is almost always kept at 1.

\begin{figure}[h]
    \centering
    \includegraphics[width=8cm]{figures/convolutional_layer.png}
    \caption{A convolution layer with multiple filters}
    \label{fig:conv_volume}
\end{figure}


\subsection{Pooling layer}

The primary purpose of the pooling layer is to down-sample feature maps. It addresses a common problem with feature maps being too sensitive to the location of the features in the input \parencite{pooling_mlm}. Also, it helps the model to generalize by reducing the complexity of the deeper layers.

The most common type of pooling method is \textit{max pooling}. It partitions the input feature map into rectangle patches and only returns the maximum from that patch, as seen in figure \ref{fig:conv_pooling} \parencite{understanding_cnns}. Another commonly used pooling operation is average pooling. It works similarly to max pooling, but it averages values of the patch instead of picking the maximum.

\begin{figure}[h]
    \centering
    \includegraphics[width=12cm]{figures/max_pooling.png}
    \caption{Illustration of max pooling}
    \label{fig:conv_pooling}
\end{figure}

\section{Transformer}

Transformer is a relatively new neural network architecture introduced in 2017 in the famous paper ``Attention is all you need'' \parencite{attention_is_all} by a team from Google. The architecture was intended initially for natural language processing applications, but later transformers were shown to be effective in other fields, like image recognition \parencite{vit}.

As the name of the paper suggests, a transformer is an ANN architecture that relies exclusively on the \textit{self-attention}, which we will discuss in the next section, to compute representations of its inputs and output. Another advantage of transformer architecture is that it allows a significantly higher degree of parallelization and thus can be trained significantly faster than previous architectures \parencite{attention_is_all}.

Transformers are structured as \textit{encoder-decoder} models. \textit{Encoder-decoder} is a type of ANN architecture that consists of two parts with unsurprising names: encoder and decoder. Encoder maps a sequence of symbol inputs to a sequence of continuous representations, sometimes referred to as code. The decoder, given the code, produces the output of the model.

Encoder, typically, receives the input in the form of \textit{embeddings}. \textit{Embeddings} are vectors of continuous values that represent discrete values. Embeddings are used to reduce the dimensions of the input. The mapping of input to vectors is learned, allowing us to map similar continuous vectors to discrete values that are semantically similar.

\subsection{Self-attention}

As we have already touched upon, the core of transformers is the \textit{self-attention}. \textit{Self-attention} receives embeddings as input and feeds them into three separate linear layers. These operations produce three distinct vectors, referred to as \textit{key}, \textit{query} and \textit{value}. \textit{Self-attention} then computes dot product of the \textit{keys} and \textit{queries}. The output of the dot product is the similarity matrix of \textit{keys} and \textit{queries} since the dot product is proportional to the cosine of two vectors. The similarity matrix is then scaled down to keep the values of the dot product from exploding. It would hurt the training performance for the next step. A \textit{softmax} is applied to the similarity matrix, and the output of that is multiplied by the value, giving us the output of the whole \textit{self-attention} block. Optionally, the attention head may include a mask. When it is applied to the similarity matrix, all values above the main diagonal are replaced with negative inf. It is done for autoregressive models so that they ignore the inputs they have not seen yet. Overall structure is described more succinctly by the left diagram in figure \ref{fig:attention_heads} from the paper \parencite{attention_is_all}. 

\begin{figure}[h]
    \centering
    \includegraphics[width=12cm]{figures/attention.png}
    \caption{Transformer blocks}
    \label{fig:attention_heads}
\end{figure}

This whole computation sounds significantly more complex, than it really is: once we have our \textit{key}, \textit{query} and \textit{value} vectors, the rest can be described with a rather simple equation in matrix form  from the original paper \parencite{attention_is_all}:

\begin{equation}
    \text{Attenton}(Q, K, V) = \text{softmax}(\frac{QK^T}{\sqrt{d_k}})V,
\end{equation}
where $V$ is the \textit{value}, $K$ is the \textit{value}, $Q$ is the \textit{value} query and $d_k$ is the dimension of the \textit{key} vector.

In transformers multiple \textit{self-attention} modules (model, proposed in the original paper uses 8) are stacked together into blocks called \textit{multi-head attention}. In \textit{multi-head attention}, outputs of all self-attention heads are concatenated together and passed through yet another linear layer, as seen on the right diagram in figure \ref{fig:attention_heads}. This allows models to learn multiple connections between the parts of the input.

Why multiple attention heads are needed is better illustrated by an example from natural language processing. Imagine that we want to compute sentiment analysis of restaurant reviews. If the model receives as input a sentence ``The food was not terrible'', the transformer has to learn a relationship between the words ``not'' and ``terrible''. This relationship will be drastically different from the connection between ``terrible'' and ``food''. Multiple heads in each attention block allow transformers to learn different relationships separately.


\section{Transfer learning}

Perhaps the most empowering concept discussed in this thesis is transfer learning. It allows us to achieve competitive performance with insufficient training data and significantly less powerful hardware. Transfer learning is a process of taking a model with all of its parameters trained on some data and leveraging it to solve a task on different data.

There are two approaches to transfer learning: using the trained model as a \textit{feature extractor} and \textit{finetuning}. \textit{Finetuning} is the more head-on approach: the output layer is replaced to fit your specific task, and the whole model is simply trained on the new data. Using model as a \textit{feature extractor} is also rather simple. It is the same procedure as the \textit{finetuning}. However, all model parameters, except for the output layer, are frozen so that they do not change in the training process. The output layer is trained to interpret the representation generated by the original model. \textit{Finetuning} with a lower \textit{learning rate} is often applied afterwards. 

Obviously, the more similar the training data used to train the model initially and the one used for the task at hand, the better the model will perform. Nevertheless, since the earlier layers in the model tend to learn simpler, more general features, transfer learning can demonstrate decent results on even dissimilar data, especially on smaller data sets.