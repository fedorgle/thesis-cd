\chapter{Implementation}
In this chapter, we will describe the practical part of the thesis. We will cover all steps of preprocessing the data, generating image data sets, and modeling, and then we will discuss the results.

\section{Used technologies}

The whole practical part of the thesis is entirely written in Python. Python is an interpreted programming language initially created by Guido van Rossum. It has taken off as the de facto standard programming language for most scientific and ML-related purposes in recent years. What sets Python apart from the other languages is an excellent ecosystem of libraries and packages for any computing task, from the web to physical simulations.

In this thesis, we heavily take advantage of Python's ecosystem throughout the implementation. Most work with data is done using the Pandas library. Pandas is an excellent data manipulation package created by Wes McKinnley that allows us to work with large tables of data. All the numeric work, like Fourier transform, is done via the NumPy library. Numpy is a numerical computing library created by Travis Oliphant. Numpy is a very performant package that adds support for n-dimensional arrays and a plethora of mathematical functions and data transformations. To compute the values of technical indicators, we used TA-Lib. TA-Lib is a popular technical analysis library widely used by trading software developers. And finally, to generate the images, we used matplotlib. Matplotlib is an extremely powerful library for creating visualizations in Python, initially created by John D. Hunter.

PyTorch handles the deep learning side of the implementation. PyTorch is a framework for deep learning models. It is a python re-implementation and extension of a Lua package called Torch, hence the name. It is primarily developed by Facebook. Since its release, PyTorch has been quickly adopted by the academic community, and it has become the most used deep learning framework.

\section{Preprocessing}

For the experiments, we have chosen an OHLC EUR/USD data set with 8 hour period, downloaded from FXCM.

\subsection{Technical indicators}

As a first step in the preprocessing pipeline, we will compute values of every technical indicator that we have discussed in the section \ref{sec:techincal_indicators} for the whole data set. Most indicators produce NaNs at the beginning of the data set, so we will have to drop those rows, which is not a big issue since those are the least relevant data points in the data set.
\subsubsection*{Used technical indicator parameter values}
\begin{itemize}
	\item \textbf{RSI}: $p = 24$
	\item \textbf{ROC}: $p = 10$
	\item \textbf{Ultimate oscillator}: $p_1 = 7$, $p_2 = 14$, $p_3 = 28$
	\item \textbf{Stochastic oscillator}: fast \%k period $p = 10$, slow \%d period $n = 3$
	\item \textbf{CCI}: $p = 24$
	\item \textbf{APO}: fast period $p_1 = 12$, slow period $p_2 = 26$
	\item \textbf{Aroon oscillator}: $p = 28$
    \item \textbf{Chande oscillator}: $p= 28$
\end{itemize}

The whole data set is then split into windows of size $w$ (for our experiments, we chose $w = 20$). Each window will be used further down the pipeline to generate a single image for our models.

\subsection{Labeling}

We have to discuss labeling in a separate subsection due to the specificity of the task. As stated in the title of the thesis, we will attempt to predict Forex signals, in other words, an upcoming change in the Forex pair's prices for each window. Nevertheless, what precisely to consider a change and for how many ticks into the future is up for debate.

There are multiple viable ways to mark a window of values as preceding a positive or negative trend. The most obvious way would be to simply consider the value right after the window and decide solely on the change in its value. An analogous approach is to pick the $n$th value after the window and label windows based on the change in its value. Finally, a more involved approach is to take $n$ values after the window, fit a line to them, and use its slope as a determining factor. All of the described approaches were used in the previous works related to our topic, but we believe that our approach will lead to better results in a real-world setting.

Our labeling method is based on nuances of the Forex domain, more specifically, how trade orders are placed and executed. Trades and orders are covered in chapter \ref{ch:forex}. We are going to have three labels: \textit{long}, \textit{short} and \textit{hold}. Both \textit{long} and \textit{short} labels imply a significant positive or negative trend in the upcoming values. On the other hand \textit{hold} means that there is no significant change in the near future prices.

To define what ``significant change'' is and how many ticks into the future is the ``near'' future, we will use two hyperparameters: $d$ and $l$, where $d$ is a threshold in pips, by which currency pair's value has to change to be considered significant, and $l$ is the number of ticks into the future, that we will consider relevant for a single window.

We are going to label a window (and all images generated from it) as \textit{long} if the first price in the next $l$ ticks that deviates from the last value of the window by more than $d$ pips is greater than the last value of the window Analogously with \textit{short} labels, but the change has to be negative. If there is not a single price in the next $l$ ticks that changes by more than $d$ pips than the last value of the window, we are going to label the window as \textit{hold}. A sample python implementation of the algorithm is provided in the code listing \ref{lst:label}.

\begin{lstlisting}[caption={Labeling algorithm in python},label={lst:label},language=Python]
import numpy as np

def get_label(close_values, w_end, l, d):
    label = "HOLD"
    diff = close_values[w_end+1:l] - close_values[w_end]
    diff_in_pips = diff * 10000 # 10000 for EUR/USD
    trigger = diff_in_pips[np.argmax(abs(diff_in_pips) > d)]
    if abs(trigger) > d: 
        label = "LONG" if trigger > 0 else "SHORT"
    return label
\end{lstlisting}

The reasoning behind this labeling approach is as follows: when traders commit to a transaction, they also place pending orders that trigger when the price increases or decreases by $d$ pips to take profit, while the situation has not worsened or as a safety net to prevent uncontrollable losses. Therefore, our labeling approach marks windows based on which order will be triggered first.

There are some fees associated with the transaction on all the trading platforms, so the point of the \textit{hold} label is to save traders money on meaningless trades when there are no opportunities for profit.

\subsection{Filtering}

When working with any market data, filtering is an essential part of the preprocessing pipeline, especially in the case of Forex, due to the sheer volume of trades happening simultaneously on the market. Filtering is essential because of large amounts of noise inside Forex data sets that obscure more general trends behind unimportant features. In this work, we are going to remove unimportant signal details using the Butterworth filter that we have covered in section \ref{sec:butter}.

Butterworth filter was chosen because it produces a smooth curve with no ripples. These qualities are precious for the next preprocessing step, where we are going to extrapolate indicators into the future, using discrete Fourier transform and for machine learning models in general since it makes input data more similar and helps models to generalize better by preventing \textit{overfitting}.


\subsection{Fourier extrapolation}

While people do draw some insight from graphs, and some traders successfully use technical indicators on their own, we are going to take it one step further by extrapolation indicators' values into the future using FFT. There are two points behind this approach's intuition. 

Firstly, a person, who is experienced in signal processing, can, with a high degree of accuracy, predict if the value of the signal will grow or shrink in the future by examining a graph of the signal's extrapolation done with FFT. We hope that ML models will be able to learn cues contained in the graphs and manage to use them to improve trend prediction.

Secondly, technical indicators are easily interpreted in hindsight, but only some traders are able to create a successful trading strategy using only technical indicators. However, by extrapolating values of the indicators into the future with at least some degree of accuracy, we can use technical indicators as a standalone trading signal, And DFT allows us to do just that.

As has already been covered in section \ref{sec:ft}, Discrete Fourier Transform allows us to decompose signals into a sum of a series of sine or cosine terms. This allows us to extend a series of values by applying FFT to them, forecasting individual components separately, and reassembling the signal by summing together the prolonged components. An example of such extrapolation on simple data can be seen in figure \ref{fig:extrapolation}.

Since Forex prices are not well-behaved by any stretch, we should expect the extrapolation to stay accurate only for a short term, so the extrapolation should only be done for some small number of ticks $r$. 

\begin{figure}[h]
    \centering
    \includegraphics[width=12cm]{figures/extrapolation.png}
    \caption{An example of FFT extrapolation}
    \label{fig:extrapolation}
\end{figure}

\subsection{Generating images}

All images that we are going to generate can be split into two groups: the ones that have thresholds that hold special meaning and the ones that do not.

For example, RSI value crossing the upper or lower threshold signifies that an asset is overbought or oversold. We want to make this additional information available to the models. Therefore when the oscillator's value crosses a threshold, we will highlight the area between the curve and the threshold line with a different color, as seen in figure \ref{fig:rsi_sample}. For all indicators with the thresholds, we chose 60 and 40 as upper and lower values instead used 30 and 70, which are commonly used for daily trading, because we are working with higher frequency data. 

For other technical indicators like ROC, we will simply color the area under the graph green if the value is greater than zero and red if it is less than zero, as seen in figure \ref{fig:roc_sample}.

All generated images are $224 \times 224$ pixels. Such specific size is dictated by the fact that $224 \times 224$ is the lowest resolution that most pre-trained models are able to accept. Samples of generated images for each indicator is displayed in figures \ref{fig:roc_sample} through \ref{fig:apo_sample}.

\begin{figure}[h]
    \centering
    \subfloat[Hold]{
        \includegraphics[width=0.3\textwidth]{figures/samples/roc/hold.png}
    }
    \subfloat[Long]{
        \includegraphics[width=0.3\textwidth]{figures/samples/roc/long.png}
    }
    \subfloat[Short]{
        \includegraphics[width=0.3\textwidth]{figures/samples/roc/short.png}
    }
    \caption{ROC sample images}
    \label{fig:roc_sample}
\end{figure}


\begin{figure}[h]
    \centering
    \subfloat[Hold]{
        \includegraphics[width=0.3\textwidth]{figures/samples/cci/hold.png}
    }
    \subfloat[Long]{
        \includegraphics[width=0.3\textwidth]{figures/samples/cci/long.png}
    }
    \subfloat[Short]{
        \includegraphics[width=0.3\textwidth]{figures/samples/cci/short.png}
    }
    \caption{CCI sample images}
\end{figure}

\begin{figure}[h]
    \centering
    \subfloat[Hold]{
        \includegraphics[width=0.3\textwidth]{figures/samples/rsi/hold.png}
    }
    \subfloat[Long]{
        \includegraphics[width=0.3\textwidth]{figures/samples/rsi/long.png}
    }
    \subfloat[Short]{
        \includegraphics[width=0.3\textwidth]{figures/samples/rsi/short.png}
    }
    \caption{RSI sample images}
    \label{fig:rsi_sample}
\end{figure}

\begin{figure}[h]
    \centering
    \subfloat[Hold]{
        \includegraphics[width=0.3\textwidth]{figures/samples/ult/hold.png}
    }
    \subfloat[Long]{
        \includegraphics[width=0.3\textwidth]{figures/samples/ult/long.png}
    }
    \subfloat[Short]{
        \includegraphics[width=0.3\textwidth]{figures/samples/ult/short.png}
    }
    \caption{Ultimate oscillator sample images}
\end{figure}

\begin{figure}[h]
    \centering
    \subfloat[Hold]{
        \label{ref_label1}
        \includegraphics[width=0.3\textwidth]{figures/samples/slow_d/hold.png}
    }
    \subfloat[Long]{
        \label{ref_label2}
        \includegraphics[width=0.3\textwidth]{figures/samples/slow_d/long.png}
    }
    \subfloat[Short]{
        \label{ref_label3}
        \includegraphics[width=0.3\textwidth]{figures/samples/slow_d/short.png}
    }
    \caption{Stochastic oscillator sample images}
\end{figure}

\begin{figure}[h]
    \centering
    \subfloat[Hold]{
        \includegraphics[width=0.3\textwidth]{figures/samples/aroon/hold.png}
    }
    \subfloat[Long]{
        \includegraphics[width=0.3\textwidth]{figures/samples/aroon/long.png}
    }
    \subfloat[Short]{
        \includegraphics[width=0.3\textwidth]{figures/samples/aroon/short.png}
    }
    \caption{Aroon oscillator sample images}
\end{figure}

\begin{figure}[h]
    \centering
    \subfloat[Hold]{
        \includegraphics[width=0.3\textwidth]{figures/samples/chande/hold.png}
    }
    \subfloat[Long]{
        \includegraphics[width=0.3\textwidth]{figures/samples/chande/long.png}
    }
    \subfloat[Short]{
        \includegraphics[width=0.3\textwidth]{figures/samples/chande/short.png}
    }
    \caption{Chande Momentum oscillator sample images}
\end{figure}

\begin{figure}[h]
    \centering
    \subfloat[Hold]{
        \includegraphics[width=0.3\textwidth]{figures/samples/apo/hold.png}
    }
    \subfloat[Long]{
        \includegraphics[width=0.3\textwidth]{figures/samples/apo/long.png}
    }
    \subfloat[Short]{
        \includegraphics[width=0.3\textwidth]{figures/samples/apo/short.png}
    }
    \caption{Absolute price oscillator sample images}
    \label{fig:apo_sample}
\end{figure}

\clearpage

\subsection{Summary}

Our preprocessing pipeline consists of the following steps:
\begin{enumerate}
    \item Compute values of technical indicators for the whole data set.
    \item Apply Butterworth filter to each indicator.
    \item Split data set into windows of size $w$.
    \item Set label for each window. Using $d$ and $l$.
    \item Extrapolate each window $r$ ticks into the future using the FFT.
    \item Create images of extrapolation's plots, as covered in the section above.
\end{enumerate}
In our experiments we landed on the following combination of parameter values: $w = 20$, $d = 50$, $l = 20$ and $r = 10$.

\section{Modeling and evaluation}

Instead of coming up with yet another neural network architecture, we are going to experiment on all the models that we have already discussed in the survey \ref{ch:survey} chapter of the thesis.

As our performance metric, we are going to simply use \textit{accuracy}. It is a simple, easy-to-understand performance benchmark and the most suitable for our problem domain. Even incorrect predictions of missing trends result in losses for traders in the form of trading platform fees. While those are not as significant as incorrect predictions of opposite trends, we should still avoid them as much as possible.

All models were trained on image data sets generated for each indicator. When splitting data into training, validation, and test sets, we made sure not to shuffle the windows. The model's performance is evaluated based on predicting the future while being trained on the examples from the past.


\begin{table}[h]
\centering
\begin{tabular}{|l|l|l|l|l|l|l|}
\hline
           & AlexNet   & VGG    & ResNet   & DenseNet & ViT    & ConvNeXt   \\ \hline
APO        & 52.0\%    & 38.6\% & 35.6\%   & 41.1\%   & 53.4\% & 50.7\%     \\ \hline
Aroon      & 45.1\%    & 31.0\% & 38.4\%   & 31.5\%   & 30.1\% & 26.0\%     \\ \hline
CCI        & 59.0\%    & 43.0\% & 45.2\%   & 52.1\%   & 46.6\% & 32.4\%     \\ \hline
Chande     & 53.0\%    & 41.0\% & 43.8\%   & 45.2\%   & 32.9\% & 27.4\%     \\ \hline
ROC        & 63.0\%    & 49.3\% & 53.4\%   & 52.1\%   & 54.8\% & 57.5\%     \\ \hline
RSI        & 60.3\%    & 46.6\% & 43.8\%   & 49.3\%   & 43.6\% & 46.6\%     \\ \hline
Stochastic & 57.0\%    & 31.5\% & 31.5\%   & 37.0\%   & 32.9\% & 30.1\%     \\ \hline
Ultimate   & 58.4\%    & 47.0\% & 45.2\%   & 46.6\%   & 34.2\% & 34.2\%     \\ \hline
\end{tabular}
\caption{Best test accuracy scores per model 8h EUR/USD}
\label{tab:acc_scores}
\end{table}

Best reached accuracy scores are summarized in table \ref{tab:acc_scores}. To achieve them, AlexNet, VGG, ResNet, and DenseNet were fine-tuned with Adam optimizer and a learning rate $\eta = 10^{-4}$. For more advanced ViT and ConvNext, we opted for AdamW \parencite{adamw}, since it is the optimizer that was used to train them in the first place, and a lower learning rate of  $\eta = 3 \times 10^{-6}$. All models were trained using \textit{cross-entropy} loss.

The highest accuracy score we achieved is $63\%$ with AlexNet and ROC images. Broader conclusions can be drawn from the heat map of the same table in figure \ref{fig:acc_heat}. 

As is evident from the heat map, ROC, the simplest out of all technical indicators, was the most successful across the board, reaching high accuracy scores across all the tested architectures. RSI attained the second-best scores. On the other hand, all models showed subpar performance when trained on images generated from Aroon oscillator.

\begin{figure}[h]
    \centering
    \includegraphics[width=14cm]{figures/acc_heat.png}
    \caption{Best test accuracy scores heatmap 8h EUR/USD}
    \label{fig:acc_heat}
\end{figure}

The best-performing architecture across all indicators is AlexNet. However, ViT and ConvNeXt displayed terrible results on most indicator data sets, with performance on par with a random classifier.

Overall, results seem to get worse with the complexity of the model. That does make sense due to the following two factors. Firstly, all generated images of the technical indicator plots are not highly complex. They contain simple geometric shapes in four colors. Therefore an exceedingly deep network might be prone to overfitting.

Secondly, there are only so many 8-hour periods in the year. Moreover, we have less than 20 years' worth of data, which is insufficient to train complex models. This especially hurts transformers, which require more data than convolutional neural networks. The situation is further worsened by the fact that we are not able to apply any data augmentation techniques to artificially increase the size of the data set since it would interfere with the semantics of the generated plots.

